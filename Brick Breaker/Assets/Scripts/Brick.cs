﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {

    public AudioClip crack;
    public Sprite[] hitSprites;
    public static int breakableBrickCount = 0;
    public GameObject smoke;

    private int timesHit;
    private LevelManager levelManager;
    private bool isBreakable;

    // Use this for initialization
    void Start () {
        levelManager = GameObject.FindObjectOfType<LevelManager>();
        timesHit = 0;
        isBreakable = (this.tag == "breakable");
        if(isBreakable)
        {
            breakableBrickCount++;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionExit2D(Collision2D collision)
    {
        AudioSource.PlayClipAtPoint(crack, transform.position);
        if (isBreakable)
        {
            HandleHit();
        }
    }

    void HandleHit()
    {
        timesHit++;

        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits)
        {
            breakableBrickCount--;
            levelManager.BrickDestroyed();
            GameObject smokePuff = Instantiate(smoke, gameObject.transform.position, Quaternion.identity);
            ParticleSystem.MainModule main = smokePuff.GetComponent<ParticleSystem>().main;
            main.startColor = gameObject.GetComponent<SpriteRenderer>().color;
            Destroy(this.gameObject);
        }
        else
        {
            LoadSprites();
        }
    }

    private void LoadSprites()
    {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex])
        {
            this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
    }
}
