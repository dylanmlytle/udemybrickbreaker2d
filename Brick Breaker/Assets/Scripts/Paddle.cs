﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{

    public bool autoPlay = false;

    private Ball ball;

    // Use this for initialization
    void Start()
    {
        ball = GameObject.FindObjectOfType<Ball>();
    }

    // Update is called once per frame
    void Update()
    {
        if (autoPlay)
        {
            AutoPlay();
        }
        else
        {
            MoveWithMouse();
        }
    }

    void MoveWithMouse()
    {
        Vector3 paddlePosition = new Vector3(0.5f, this.transform.position.y, 0f);
        float mousePositionInBlocks = Input.mousePosition.x / Screen.width * 16;
        paddlePosition.x = Mathf.Clamp(mousePositionInBlocks, 0.5f, 15.5f);
        this.transform.position = paddlePosition;
    }

    void AutoPlay()
    {
        this.transform.position = new Vector3(ball.transform.position.x, transform.position.y, transform.position.z);
    }
}
