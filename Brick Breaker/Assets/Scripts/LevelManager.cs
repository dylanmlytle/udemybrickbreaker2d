﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void LoadScene(string name) {
        Brick.breakableBrickCount = 0;
        SceneManager.LoadScene(name);
    }

    public void LoadNextScene()
    {
        Brick.breakableBrickCount = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void BrickDestroyed()
    {
        if(Brick.breakableBrickCount <= 0)
        {
            LoadNextScene();
        }
    }
}
